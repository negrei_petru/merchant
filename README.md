---
Title: Online shopping
Function: Ecomence 
---

In this project we will build an e-commerce site for a small grocery that wants to sell products directly to customers over the web. 

You can read all the information about User Guide [here](https://bitbucket.org/negrei_petru/merchant/raw/master/docs/user_guide.pdf)

Or you can read all the documentation regarding the project [here](https://bitbucket.org/negrei_petru/merchant/raw/master/docs/documentation.pdf)

****

## Technical part

To start running this project on your machine you need to execute the following sequence of commands in your command line in the project folder.


```

bundle install
rake db:migrate
rake sunspot:solr:start
rails s


```