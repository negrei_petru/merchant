class Product < ActiveRecord::Base
  has_many :images, :dependent => :destroy
  accepts_nested_attributes_for :images, :allow_destroy => true

  has_many :order_items
  before_destroy :ensure_not_referenced_by_any_order_item

  has_many :categorizations
  has_many :categories, through: :categorizations
  
  private

  searchable do
     text :title, :description
  end

  # ensure that there are no line items referencing this product
  def ensure_not_referenced_by_any_order_item
    if order_items.empty?
      return true
    else
      errors.add(:base, "Order Items present. You can't delete this item")
      return false
    end
  end
end
