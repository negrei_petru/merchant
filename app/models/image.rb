class Image < ActiveRecord::Base
  belongs_to :product
  has_attached_file :photo,
    :styles => {
      :thumb=> "100x100#",
      :list => "200x200",
      :small  => "300x300>",
      :large => "600x600>"
    }, default_url: '/assets/missing.png'
  do_not_validate_attachment_file_type :photo
end
