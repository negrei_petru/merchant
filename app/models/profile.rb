class Profile < ActiveRecord::Base
  SHIPPING_TYPES = [ "Cheapest", "Fastest"]

  belongs_to :user
  has_attached_file :avatar, :styles =>{:thumb => "100x100>",:small => "200x200>", :medium => "280x280>"}, default_url: '/assets/default_avatar.png'
  do_not_validate_attachment_file_type :avatar
  
  #validates :full_name, presence:true
  #validates :user_name, presence:true
end
