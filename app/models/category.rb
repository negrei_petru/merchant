class Category < ActiveRecord::Base
  has_many :categorizations
  has_many :products, through: :categorizations
  
  has_ancestry

  def self.leaves
    self.all.inject([]) { |res, item| res << item if item.is_childless?; res }
  end

end
