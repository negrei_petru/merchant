class User < ActiveRecord::Base
  has_secure_password

  validates :email, presence: true, uniqueness: true
  before_create { generate_token(:auth_token) }

  has_one :profile, dependent: :destroy
  has_many :orders
  has_one :cart
  delegate :full_name, to: :profile, allow_nil: true
  delegate :user_name, to: :profile, allow_nil: true
  
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end
end
