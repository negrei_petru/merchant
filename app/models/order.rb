class Order < ActiveRecord::Base
  PAYMENT_TYPES = [ "Check", "Credit card", "Purchase order" ]

  has_many :order_items, dependent: :destroy
  belongs_to :user
  before_save :set_due_date, :set_status

  validates :pay_type, inclusion: PAYMENT_TYPES

  def set_due_date
    self.due_date = Time.now + (1*7*24*60*60)
  end

  def set_status
    self.status = "Shipping"
  end

  def add_line_items_from_cart(cart)
    cart.order_items.each do |item|
      item.cart_id = nil
      order_items << item
    end
  end
  
  def total_price
    order_items.to_a.sum { |item| item.total_price }
  end

  def tax_price
    (total_price*9.3)/100
  end

  def shipping_price
    5.80
  end

  def final_price
    total_price+tax_price+shipping_price
  end

  def self.chart_data(start = 3.weeks.ago)
    total_prices = prices_by_day(start)
    # shipping_prices = where(status: "Shipping").prices_by_day(start)
    (start.to_date..Date.today).map do |date|
      {
        created_at: date,
        price: total_prices[date] || 0,
        # shipping_price: shipping_prices[date] || 0,
      }
    end
  end

  def self.prices_by_day(start)
    orders = where(created_at: start.beginning_of_day..Time.zone.now)
    orders = orders.group("date(created_at)")
    orders.each_with_object({}) do |order, prices|
      prices[order.created_at.to_date] = order.total_price
    end
  end

end
