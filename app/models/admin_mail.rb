class AdminMail < ActiveRecord::Base
  before_save :default_values

  def default_values
    self.at = Time.now
  end

  def self.deliver(message)
    AdminMail.create!(
      subject: message.subject, 
      body: message.text_part.body.decoded,
      from: message.from.first, 
      at: message.date, 
      status: "inbox"
    )
  end

  def self.received
    where(status: 'inbox').order("created_at desc")
  end

  def self.sent
    where(status: 'sent').order("created_at desc")
  end
end
