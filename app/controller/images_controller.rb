class ImagesController < ApplicationController
  before_filter :product

  def index
    @images = @product.images.all
  end
 
  def show
    @image = @product.images.find(params[:id])
  end
 
  def new
    @image = @product.images.new
  end

  def edit
    @image = @product.images.find(params[:id])
  end

  def create
    @image = @product.images.new(image_params)
    if @image.save!
      flash[:success] =  "Image was added successefully."
      redirect_to @product
    else
      flash[:error] = "There was an problem."
      redirect_to :new
    end
  end
 
  def update
    @image = @product.images.find(params[:id])
    if @image.update_attributes(image_params)
      redirect_to @product
    end
  end

  def destroy
    @image = @product.images.find(params[:id])
    @image.destroy

    redirect_to edit_product_path(@product)
  end
 
  private
  def product
    @product ||= Product.find params[:product_id]
  end

  def product_params
    params.require(:product).permit(:images, :id)
  end

  def image_params
    params.require(:image).permit(:photo)
  end

end