class UsersController < ApplicationController
  before_filter :set_user, only: [:edit, :update]
  before_filter :authenticate!, only: [:edit, :update]

  def new
    @user = User.new
    @profile = Profile.new
  end

  def create
    @user = User.new user_params
    @profile = Profile.new profile_params

    if @user.valid? && @profile.valid?
      @user.profile = @profile
      @user.save!
      cookies[:auth_token] = @user.auth_token
      redirect_to root_url
      flash[:success] = "Thank you for signing in."
    else
      @profile.valid? # need to be changed
      flash[:error] = "Coundn't signup."
      render :new
    end
  end

  def edit
    @profile = @user.profile || Profile.new
  end

  def update
    @user.build_profile
    @user.assign_attributes(user_params)
    @user.profile.assign_attributes(profile_params)

    if @user.valid? && @user.profile.valid?
      @user.save
      flash[:success] = "Updated the profile successefully"
      redirect_to root_url
    else
      @user.profile.valid?
      @profile = @user.profile
      flash[:error] = "Coudn't update the user information"
      render :edit
    end
  end

  private

  def set_user
    @user ||= User.find params[:id]
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :profile)
  end

  def profile_params
    params.require(:profile).permit(:full_name, :user_name, :phone, :address, :shipping_info, :credit_info, :pay_type)
  end
end
