$ ->
  # enable chosen js
  $('#product_category_ids').chosen
    allow_single_deselect:true
    no_results_text: 'No results matched'
    width: '100%'