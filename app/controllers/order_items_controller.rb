class OrderItemsController < ApplicationController
  before_action :set_cart, only: [:create]
  before_action :set_order_item, only: [:destroy]

  def create
    product = Product.find(params[:product_id])
    @order_item = @cart.add_product(product.id)

    respond_to do |format|
      if @order_item.save
        format.html do
          redirect_to products_path
          flash[:success] = 'Order item was successfully created and added to the cart.'
        end
        format.js
      else
        flash[:error] = "Couldn't add the product to the cart."
        render :new
      end
    end
  end

  def destroy
    respond_to do |format|
      @order_item.destroy
      format.html do
        flash[:success] = "The item removed from cart"
        redirect_to products_path
      end
      format.js
    end
  end

  private
    def set_order_item
      @order_item = OrderItem.find(params[:id])
    end

    def order_item_params
      params.require(:order_item).permit(:product_id, :cart_id, :quantity)
    end
end
