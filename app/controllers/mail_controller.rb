class MailController < ApplicationController
  before_action :all_emails, only: [:index, :sent]

  def index
    @emails = AdminMail.received.page(params[:page]).per(10)
  end

  def create
    @email = AdminMail.new(mail_params)
    # bad (temporary)
    @email.status = "sent"
    @email.from = "appoYearProject@gmail.com" 
    if @email.save
      flash[:success] = 'Email was successfully send.'
      AdminMailer.message_sending(@email).deliver
    else
      flash[:error] = "There was an error"
    end
    redirect_to root_path
  end
  
  def sent
    @emails = AdminMail.sent.page(params[:page]).per(10)
    render :index
  end

  private

  # not the best solution
  def all_emails
    @all_emails = AdminMail.all
  end

  def mail_params
    params.require(:admin_mail).permit(:user_id, :subject, :body, :to, :from, :at, :status)
  end

end

