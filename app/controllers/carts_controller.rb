class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :destroy]

  def show
  end

  def destroy
    @cart.destroy
    session[:cart_id] = nil
    redirect_to products_path
    flash[:success] = 'Your cart is currently empty.'
  end

  private
    def set_cart
      @cart = Cart.find(params[:id])
    end

    def cart_params
      params[:cart]
    end
end
