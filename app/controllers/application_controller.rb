class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  before_filter :set_cart
  private

  def set_cart
    @cart = Cart.find_or_initialize_by(id: session[:cart_id])
    if @cart.new_record?
      @cart.save!
      session[:cart_id] = @cart.id
    end
  end
  
  def current_user
    @current_user ||= User.find_by_auth_token(cookies[:auth_token]) if cookies[:auth_token]
  end

  def authenticate!
    redirect_to login_path unless current_user
  end
  
end
