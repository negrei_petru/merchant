class ProductsController < ApplicationController
  authorize_resource only: [:new, :edit, :update, :destroy]
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @search = Product.search do 
      fulltext params[:search]
    end
    @products = @search.results
    # @products = Product.all
  end

  def show
  end

  def new
    @product = Product.new
    @product.images.build
    #5.times { @product.images.build }
  end

  def edit
    @product.images.build
    #5.times { @product.images.build }
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = 'Product was successfully created.'
      redirect_to @product
    else
      flash[:error] = "Product wasn't created."
      render :new 
    end
  end

  def update
    respond_to do |format|
      if @product.update_attributes(product_params)
        format.html { redirect_to @product  }
        format.json { head :update }
      else
        render :edit
      end
    end
  end

  def destroy
    if @product.destroy 
      flash[:success] = "The products was successfully deleted"
    else
      flash[:error] = @product.errors[:base].first
    end
    redirect_to products_url
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:title, :description, :price, :category_ids => [], images_attributes: [ :id, :photo ])
    end
end