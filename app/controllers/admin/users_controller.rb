class Admin::UsersController < ApplicationController
  authorize_resource :class => false

  def index
    @users = User.all
  end
end
