class Admin::DashboardController < ApplicationController
  authorize_resource :class => false

  def index
    @users = User.all
    @products = Product.all
    @orders = Order.all
    @sales = Order.where(status:'shipped');
  end
end
