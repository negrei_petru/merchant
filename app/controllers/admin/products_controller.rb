class Admin::ProductsController < ApplicationController
  authorize_resource :class => false

  def index
    @products = Product.all
  end

end