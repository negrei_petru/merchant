class AdminMailer < ActionMailer::Base
  default from: "appoYearProject@gmail.com"

  def message_sending(mail)
    @mail = mail
    mail(:to => mail.to, :subject => mail.subject)
  end
end
