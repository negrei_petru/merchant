class AddPhoneAndAddressAndShippingInfoAndCretitInfoAndPayTypeToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :phone, :string
    add_column :profiles, :address, :string
    add_column :profiles, :shipping_info, :string
    add_column :profiles, :credit_info, :string
    add_column :profiles, :pay_type, :string
  end
end
