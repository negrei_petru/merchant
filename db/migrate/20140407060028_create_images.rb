class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :product_id
      t.string  :photo_file_name

      t.timestamps
    end
  end
end
