class CreateIncomingMails < ActiveRecord::Migration
  def change
    create_table :incoming_mails do |t|
      t.integer :user_id
      t.string :subject
      t.text :body
      t.string :from
      t.datetime :at

      t.timestamps
    end
  end
end
