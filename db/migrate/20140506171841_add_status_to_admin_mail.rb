class AddStatusToAdminMail < ActiveRecord::Migration
  def change
    add_column :admin_mails, :status, :string
    add_column :admin_mails, :to, :string
  end
end
