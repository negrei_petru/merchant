class RenameIncommingMailToAdminEmail < ActiveRecord::Migration
  def change
    rename_table :incoming_mails, :admin_mails
  end
end
