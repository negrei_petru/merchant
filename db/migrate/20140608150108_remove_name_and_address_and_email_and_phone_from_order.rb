class RemoveNameAndAddressAndEmailAndPhoneFromOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :name, :string
    remove_column :orders, :address, :text
    remove_column :orders, :email, :string
    remove_column :orders, :phone, :string
  end
end
